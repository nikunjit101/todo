<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class note
 * @package App\Models
 * @version July 31, 2017, 10:49 am UTC
 *
 * @method static note find($id=null, $columns = array())
 * @method static note|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property integer task_id
 * @property string details
 * @property integer created_by
 */
class note extends Model
{
    use SoftDeletes;

    public $table = 'notes';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'task_id',
        'details',
        'created_by'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'task_id' => 'integer',
        'details' => 'string',
        'created_by' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'task_id' => 'required',
        'details' => 'required'
    ];

    
}
