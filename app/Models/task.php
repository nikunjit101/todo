<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class task
 * @package App\Models
 * @version July 31, 2017, 10:34 am UTC
 *
 * @method static task find($id=null, $columns = array())
 * @method static task|\Illuminate\Database\Eloquent\Collection findOrFail($id, $columns = ['*'])
 * @property string title
 * @property string description
 * @property integer project_id
 * @property integer status
 * @property integer urgent_level
 * @property integer important_level
 * @property integer created_by
 * @property integer assign_to
 * @property timestamp due_date
 */
class task extends Model
{
    use SoftDeletes;

    public $table = 'tasks';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'title',
        'description',
        'project_id',
        'status',
        'urgent_level',
        'important_level',
        'created_by',
        'assign_to',
        'due_date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'description' => 'string',
        'project_id' => 'integer',
        'status' => 'integer',
        'urgent_level' => 'integer',
        'important_level' => 'integer',
        'created_by' => 'integer',
        'assign_to' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        'description' => 'required',
        'project_id' => 'required',
        'status' => 'required',
        'created_by' => 'required',
        'assign_to' => 'required',
        'due_date' => 'required'
    ];

    
}
