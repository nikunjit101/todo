<?php

namespace App\Repositories;

use App\Models\task;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class taskRepository
 * @package App\Repositories
 * @version July 31, 2017, 10:34 am UTC
 *
 * @method task findWithoutFail($id, $columns = ['*'])
 * @method task find($id, $columns = ['*'])
 * @method task first($columns = ['*'])
*/
class taskRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'description',
        'project_id',
        'status',
        'urgent_level',
        'important_level',
        'created_by',
        'assign_to',
        'due_date'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return task::class;
    }
}
