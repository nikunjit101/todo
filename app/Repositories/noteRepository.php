<?php

namespace App\Repositories;

use App\Models\note;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class noteRepository
 * @package App\Repositories
 * @version July 31, 2017, 10:49 am UTC
 *
 * @method note findWithoutFail($id, $columns = ['*'])
 * @method note find($id, $columns = ['*'])
 * @method note first($columns = ['*'])
*/
class noteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'task_id',
        'details',
        'created_by'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return note::class;
    }
}
