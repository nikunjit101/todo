<?php

namespace App\Repositories;

use App\Models\project;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class projectRepository
 * @package App\Repositories
 * @version July 31, 2017, 10:07 am UTC
 *
 * @method project findWithoutFail($id, $columns = ['*'])
 * @method project find($id, $columns = ['*'])
 * @method project first($columns = ['*'])
*/
class projectRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'color'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return project::class;
    }
}
