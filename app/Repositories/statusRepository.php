<?php

namespace App\Repositories;

use App\Models\status;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class statusRepository
 * @package App\Repositories
 * @version July 31, 2017, 10:28 am UTC
 *
 * @method status findWithoutFail($id, $columns = ['*'])
 * @method status find($id, $columns = ['*'])
 * @method status first($columns = ['*'])
*/
class statusRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'is_system'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return status::class;
    }
}
