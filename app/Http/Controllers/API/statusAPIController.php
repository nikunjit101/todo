<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatestatusAPIRequest;
use App\Http\Requests\API\UpdatestatusAPIRequest;
use App\Models\status;
use App\Repositories\statusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class statusController
 * @package App\Http\Controllers\API
 */

class statusAPIController extends AppBaseController
{
    /** @var  statusRepository */
    private $statusRepository;

    public function __construct(statusRepository $statusRepo)
    {
        $this->statusRepository = $statusRepo;
    }

    /**
     * Display a listing of the status.
     * GET|HEAD /statuses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->statusRepository->pushCriteria(new RequestCriteria($request));
        $this->statusRepository->pushCriteria(new LimitOffsetCriteria($request));
        $statuses = $this->statusRepository->all();

        return $this->sendResponse($statuses->toArray(), 'Statuses retrieved successfully');
    }

    /**
     * Store a newly created status in storage.
     * POST /statuses
     *
     * @param CreatestatusAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatestatusAPIRequest $request)
    {
        $input = $request->all();

        $statuses = $this->statusRepository->create($input);

        return $this->sendResponse($statuses->toArray(), 'Status saved successfully');
    }

    /**
     * Display the specified status.
     * GET|HEAD /statuses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var status $status */
        $status = $this->statusRepository->findWithoutFail($id);

        if (empty($status)) {
            return $this->sendError('Status not found');
        }

        return $this->sendResponse($status->toArray(), 'Status retrieved successfully');
    }

    /**
     * Update the specified status in storage.
     * PUT/PATCH /statuses/{id}
     *
     * @param  int $id
     * @param UpdatestatusAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatestatusAPIRequest $request)
    {
        $input = $request->all();

        /** @var status $status */
        $status = $this->statusRepository->findWithoutFail($id);

        if (empty($status)) {
            return $this->sendError('Status not found');
        }

        $status = $this->statusRepository->update($input, $id);

        return $this->sendResponse($status->toArray(), 'status updated successfully');
    }

    /**
     * Remove the specified status from storage.
     * DELETE /statuses/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var status $status */
        $status = $this->statusRepository->findWithoutFail($id);

        if (empty($status)) {
            return $this->sendError('Status not found');
        }

        $status->delete();

        return $this->sendResponse($id, 'Status deleted successfully');
    }
}
