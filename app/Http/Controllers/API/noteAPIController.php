<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatenoteAPIRequest;
use App\Http\Requests\API\UpdatenoteAPIRequest;
use App\Models\note;
use App\Repositories\noteRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class noteController
 * @package App\Http\Controllers\API
 */

class noteAPIController extends AppBaseController
{
    /** @var  noteRepository */
    private $noteRepository;

    public function __construct(noteRepository $noteRepo)
    {
        $this->noteRepository = $noteRepo;
    }

    /**
     * Display a listing of the note.
     * GET|HEAD /notes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->noteRepository->pushCriteria(new RequestCriteria($request));
        $this->noteRepository->pushCriteria(new LimitOffsetCriteria($request));
        $notes = $this->noteRepository->all();

        return $this->sendResponse($notes->toArray(), 'Notes retrieved successfully');
    }

    /**
     * Store a newly created note in storage.
     * POST /notes
     *
     * @param CreatenoteAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatenoteAPIRequest $request)
    {
        $input = $request->all();

        $notes = $this->noteRepository->create($input);

        return $this->sendResponse($notes->toArray(), 'Note saved successfully');
    }

    /**
     * Display the specified note.
     * GET|HEAD /notes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var note $note */
        $note = $this->noteRepository->findWithoutFail($id);

        if (empty($note)) {
            return $this->sendError('Note not found');
        }

        return $this->sendResponse($note->toArray(), 'Note retrieved successfully');
    }

    /**
     * Update the specified note in storage.
     * PUT/PATCH /notes/{id}
     *
     * @param  int $id
     * @param UpdatenoteAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatenoteAPIRequest $request)
    {
        $input = $request->all();

        /** @var note $note */
        $note = $this->noteRepository->findWithoutFail($id);

        if (empty($note)) {
            return $this->sendError('Note not found');
        }

        $note = $this->noteRepository->update($input, $id);

        return $this->sendResponse($note->toArray(), 'note updated successfully');
    }

    /**
     * Remove the specified note from storage.
     * DELETE /notes/{id}
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var note $note */
        $note = $this->noteRepository->findWithoutFail($id);

        if (empty($note)) {
            return $this->sendError('Note not found');
        }

        $note->delete();

        return $this->sendResponse($id, 'Note deleted successfully');
    }
}
