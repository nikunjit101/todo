<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('auth/register', 'userController@register');
Route::post('auth/login', 'userController@login');
Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('user', 'userController@getAuthUser');

    Route::resource('projects', 'projectAPIController');

    Route::resource('statuses', 'statusAPIController');

    Route::resource('tasks', 'taskAPIController');

    Route::resource('notes', 'noteAPIController');
});



