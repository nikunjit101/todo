<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatestatusesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->tinyInteger('is_system')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });

        $statuses = [
            "Completed",
            "Active"
        ];

        foreach ($statuses as $key => $value) {
            \App\Models\Status::create([
                'name' => $value,
                "is_system" => true
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('statuses');
    }
}
