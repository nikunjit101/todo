<?php

use App\Models\status;
use App\Repositories\statusRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class statusRepositoryTest extends TestCase
{
    use MakestatusTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var statusRepository
     */
    protected $statusRepo;

    public function setUp()
    {
        parent::setUp();
        $this->statusRepo = App::make(statusRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatestatus()
    {
        $status = $this->fakestatusData();
        $createdstatus = $this->statusRepo->create($status);
        $createdstatus = $createdstatus->toArray();
        $this->assertArrayHasKey('id', $createdstatus);
        $this->assertNotNull($createdstatus['id'], 'Created status must have id specified');
        $this->assertNotNull(status::find($createdstatus['id']), 'status with given id must be in DB');
        $this->assertModelData($status, $createdstatus);
    }

    /**
     * @test read
     */
    public function testReadstatus()
    {
        $status = $this->makestatus();
        $dbstatus = $this->statusRepo->find($status->id);
        $dbstatus = $dbstatus->toArray();
        $this->assertModelData($status->toArray(), $dbstatus);
    }

    /**
     * @test update
     */
    public function testUpdatestatus()
    {
        $status = $this->makestatus();
        $fakestatus = $this->fakestatusData();
        $updatedstatus = $this->statusRepo->update($fakestatus, $status->id);
        $this->assertModelData($fakestatus, $updatedstatus->toArray());
        $dbstatus = $this->statusRepo->find($status->id);
        $this->assertModelData($fakestatus, $dbstatus->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletestatus()
    {
        $status = $this->makestatus();
        $resp = $this->statusRepo->delete($status->id);
        $this->assertTrue($resp);
        $this->assertNull(status::find($status->id), 'status should not exist in DB');
    }
}
