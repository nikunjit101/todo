<?php

use App\Models\note;
use App\Repositories\noteRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class noteRepositoryTest extends TestCase
{
    use MakenoteTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var noteRepository
     */
    protected $noteRepo;

    public function setUp()
    {
        parent::setUp();
        $this->noteRepo = App::make(noteRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatenote()
    {
        $note = $this->fakenoteData();
        $creatednote = $this->noteRepo->create($note);
        $creatednote = $creatednote->toArray();
        $this->assertArrayHasKey('id', $creatednote);
        $this->assertNotNull($creatednote['id'], 'Created note must have id specified');
        $this->assertNotNull(note::find($creatednote['id']), 'note with given id must be in DB');
        $this->assertModelData($note, $creatednote);
    }

    /**
     * @test read
     */
    public function testReadnote()
    {
        $note = $this->makenote();
        $dbnote = $this->noteRepo->find($note->id);
        $dbnote = $dbnote->toArray();
        $this->assertModelData($note->toArray(), $dbnote);
    }

    /**
     * @test update
     */
    public function testUpdatenote()
    {
        $note = $this->makenote();
        $fakenote = $this->fakenoteData();
        $updatednote = $this->noteRepo->update($fakenote, $note->id);
        $this->assertModelData($fakenote, $updatednote->toArray());
        $dbnote = $this->noteRepo->find($note->id);
        $this->assertModelData($fakenote, $dbnote->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletenote()
    {
        $note = $this->makenote();
        $resp = $this->noteRepo->delete($note->id);
        $this->assertTrue($resp);
        $this->assertNull(note::find($note->id), 'note should not exist in DB');
    }
}
