<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class statusApiTest extends TestCase
{
    use MakestatusTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatestatus()
    {
        $status = $this->fakestatusData();
        $this->json('POST', '/api/v1/statuses', $status);

        $this->assertApiResponse($status);
    }

    /**
     * @test
     */
    public function testReadstatus()
    {
        $status = $this->makestatus();
        $this->json('GET', '/api/v1/statuses/'.$status->id);

        $this->assertApiResponse($status->toArray());
    }

    /**
     * @test
     */
    public function testUpdatestatus()
    {
        $status = $this->makestatus();
        $editedstatus = $this->fakestatusData();

        $this->json('PUT', '/api/v1/statuses/'.$status->id, $editedstatus);

        $this->assertApiResponse($editedstatus);
    }

    /**
     * @test
     */
    public function testDeletestatus()
    {
        $status = $this->makestatus();
        $this->json('DELETE', '/api/v1/statuses/'.$status->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/statuses/'.$status->id);

        $this->assertResponseStatus(404);
    }
}
