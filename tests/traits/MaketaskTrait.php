<?php

use Faker\Factory as Faker;
use App\Models\task;
use App\Repositories\taskRepository;

trait MaketaskTrait
{
    /**
     * Create fake instance of task and save it in database
     *
     * @param array $taskFields
     * @return task
     */
    public function maketask($taskFields = [])
    {
        /** @var taskRepository $taskRepo */
        $taskRepo = App::make(taskRepository::class);
        $theme = $this->faketaskData($taskFields);
        return $taskRepo->create($theme);
    }

    /**
     * Get fake instance of task
     *
     * @param array $taskFields
     * @return task
     */
    public function faketask($taskFields = [])
    {
        return new task($this->faketaskData($taskFields));
    }

    /**
     * Get fake data of task
     *
     * @param array $postFields
     * @return array
     */
    public function faketaskData($taskFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title' => $fake->word,
            'description' => $fake->text,
            'project_id' => $fake->randomDigitNotNull,
            'status' => $fake->randomDigitNotNull,
            'urgent_level' => $fake->randomDigitNotNull,
            'important_level' => $fake->randomDigitNotNull,
            'created_by' => $fake->randomDigitNotNull,
            'assign_to' => $fake->randomDigitNotNull,
            'due_date' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $taskFields);
    }
}
