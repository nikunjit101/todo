<?php

use Faker\Factory as Faker;
use App\Models\status;
use App\Repositories\statusRepository;

trait MakestatusTrait
{
    /**
     * Create fake instance of status and save it in database
     *
     * @param array $statusFields
     * @return status
     */
    public function makestatus($statusFields = [])
    {
        /** @var statusRepository $statusRepo */
        $statusRepo = App::make(statusRepository::class);
        $theme = $this->fakestatusData($statusFields);
        return $statusRepo->create($theme);
    }

    /**
     * Get fake instance of status
     *
     * @param array $statusFields
     * @return status
     */
    public function fakestatus($statusFields = [])
    {
        return new status($this->fakestatusData($statusFields));
    }

    /**
     * Get fake data of status
     *
     * @param array $postFields
     * @return array
     */
    public function fakestatusData($statusFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'is_system' => $fake->text,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $statusFields);
    }
}
