<?php

use Faker\Factory as Faker;
use App\Models\note;
use App\Repositories\noteRepository;

trait MakenoteTrait
{
    /**
     * Create fake instance of note and save it in database
     *
     * @param array $noteFields
     * @return note
     */
    public function makenote($noteFields = [])
    {
        /** @var noteRepository $noteRepo */
        $noteRepo = App::make(noteRepository::class);
        $theme = $this->fakenoteData($noteFields);
        return $noteRepo->create($theme);
    }

    /**
     * Get fake instance of note
     *
     * @param array $noteFields
     * @return note
     */
    public function fakenote($noteFields = [])
    {
        return new note($this->fakenoteData($noteFields));
    }

    /**
     * Get fake data of note
     *
     * @param array $postFields
     * @return array
     */
    public function fakenoteData($noteFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'task_id' => $fake->randomDigitNotNull,
            'details' => $fake->text,
            'created_by' => $fake->randomDigitNotNull,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $noteFields);
    }
}
